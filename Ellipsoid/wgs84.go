package ellipsoid

import (
	"math"
)

type WGS84 struct {}

func (wgs84 WGS84) Flattening() float64 {
    return 298.257223563
}

func (wgs84 WGS84) InverseFlattening() float64 {
    return 1 / wgs84.Flattening()
}

func (wgs84 WGS84) SemiMajorAxis() float64 {
    return 6378137.0
}

func (wgs84 WGS84) SemiMinorAxis() float64 {
    return wgs84.SemiMajorAxis() * (1 - wgs84.InverseFlattening())
}

func (wgs84 WGS84) FirstEccentricitySquared() float64 {
    return (2 * wgs84.InverseFlattening()) - math.Pow(wgs84.InverseFlattening(), 2);
}

func (wgs84 WGS84) SecondEccentricitySquared() float64 {
    return wgs84.InverseFlattening() * wgs84.FirstEccentricitySquared();
}