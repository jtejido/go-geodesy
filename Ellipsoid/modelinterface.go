package ellipsoid

type ModelInterface interface {
	Flattening() float64
	InverseFlattening() float64
	SemiMajorAxis() float64
	SemiMinorAxis() float64
	FirstEccentricitySquared() float64
	SecondEccentricitySquared() float64
}
