package ellipsoid

import (
	"math"
)

type Clarke1866 struct {}

func (clarke1866 Clarke1866) Flattening() float64 {
    return 294.9786982139006
}

func (clarke1866 Clarke1866) InverseFlattening() float64 {
    return 1 / clarke1866.Flattening()
}

func (clarke1866 Clarke1866) SemiMajorAxis() float64 {
    return 6378206.4
}

func (clarke1866 Clarke1866) SemiMinorAxis() float64 {
    return clarke1866.SemiMajorAxis() * (1 - clarke1866.InverseFlattening())
}

func (clarke1866 Clarke1866) FirstEccentricitySquared() float64 {
    return (2 * clarke1866.InverseFlattening()) - math.Pow(clarke1866.InverseFlattening(), 2);
}

func (clarke1866 Clarke1866) SecondEccentricitySquared() float64 {
    return clarke1866.InverseFlattening() * clarke1866.FirstEccentricitySquared();
}