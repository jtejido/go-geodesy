package datum

import (
	ellipsoid "bitbucket.org/jtejido/go-geodesy/Ellipsoid"
)

type WGS84 struct {
	EllipsoidModel ellipsoid.WGS84
}

func (wgs84 WGS84) SemiMajorAxis() float64 {
	return wgs84.EllipsoidModel.SemiMajorAxis()
}

func (wgs84 WGS84) SemiMinorAxis() float64 {
	return wgs84.EllipsoidModel.SemiMinorAxis()
}

func (wgs84 WGS84) InverseFlattening() float64 {
	return wgs84.EllipsoidModel.InverseFlattening()
}

func (wgs84 WGS84) Parameters(toWGS bool) map[string]float64 {

	var array = map[string]float64{"tx": 0.0, "ty": 0.0, "tz": 0.0, "rx": 0.0, "ry": 0.0, "rz": 0.0, "s": 0.0}
	if toWGS {
		x := make(map[string]float64)
		for k, v := range array {
			x[k] = v * -1
	    }
	    array = x
	}

	return array
}

func (wgs84 WGS84) Name() string {
	return "World Geodetic System 1984"
}

func (wgs84 WGS84) Remarks() string {
    return ""
}

func (wgs84 WGS84) Source() string {
    return "NIMA TR8350.2 January 2000 revision. http://earth-info.nga.mil/GandG/"
}

func (wgs84 WGS84) Scope() string {
    return "Used by the GPS satellite navigation system and for NATO military geodetic surveying."
}

func (wgs84 WGS84) Origin() string {
    return "Earth’s center of mass being defined for the whole Earth including oceans and atmosphere. "
}

func (wgs84 WGS84) CRSCode() int {
    return 4326
}

func (wgs84 WGS84) Area() string {
    return "World"
}

func (wgs84 WGS84) Model() ellipsoid.ModelInterface {
    return wgs84.EllipsoidModel
}