package datum

import (
	ellipsoid "bitbucket.org/jtejido/go-geodesy/Ellipsoid"
)

type DatumInterface interface {
	Parameters(toWgs bool) map[string]float64
	Name() string
	Remarks() string
	Source() string
	Scope() string
	Origin() string
	CRSCode() int
	Area() string
	Model() ellipsoid.ModelInterface
	SemiMajorAxis() float64
	SemiMinorAxis() float64
	InverseFlattening() float64
}
