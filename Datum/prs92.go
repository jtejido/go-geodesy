package datum

import (
	ellipsoid "bitbucket.org/jtejido/go-geodesy/Ellipsoid"
)

type PRS92 struct {
	EllipsoidModel ellipsoid.Clarke1866
}

func (prs92 PRS92) SemiMajorAxis() float64 {
	return prs92.EllipsoidModel.SemiMajorAxis()
}

func (prs92 PRS92) SemiMinorAxis() float64 {
	return prs92.EllipsoidModel.SemiMinorAxis()
}

func (prs92 PRS92) InverseFlattening() float64 {
	return prs92.EllipsoidModel.InverseFlattening()
}

func (prs92 PRS92) Parameters(toWGS bool) map[string]float64 {

	var array = map[string]float64{"tx": 127.62, "ty": 67.24, "tz": 47.04, "rx": 3.068, "ry": -4.903, "rz": -1.578, "s": 1.06}
	if toWGS {
		x := make(map[string]float64)
		for k, v := range array {
			x[k] = v * -1
	    }
	    array = x
	}

	return array
}

func (prs92 PRS92) Name() string {
	return "Philippine Reference System 1992"
}

func (prs92 PRS92) Remarks() string {
    return "Derived during GPS campaign which established PRS92 coordinates at 330 first order stations."
}

func (prs92 PRS92) Source() string {
    return "National Mapping and Resource Information Authority, Coast and Geodetic Survey Department."
}

func (prs92 PRS92) Scope() string {
    return "Accuracy: 1-10 parts per million."
}

func (prs92 PRS92) Origin() string {
    return "Fundamental point: Balacan. Latitude: 13 degrees 33 minutes 41.000 seconds N, longitude: 121 degrees 52 minutes 03.000 seconds E (of Greenwich), geoid-ellipsoid separation 0.34m."
}

func (prs92 PRS92) CRSCode() int {
    return 4683
}

func (prs92 PRS92) Area() string {
    return "Philippines"
}

func (prs92 PRS92) Model() ellipsoid.ModelInterface {
    return prs92.EllipsoidModel
}