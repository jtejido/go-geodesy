package transformation

import (
	. "bitbucket.org/jtejido/go-geodesy/Location"
	datum "bitbucket.org/jtejido/go-geodesy/Datum"
    "bitbucket.org/jtejido/go-geodesy/Unit"
)

type Helmert struct {
	Location Geographic
	DestinationDatum datum.DatumInterface
    BaseTransformation
    IsCFR bool
}

func (helmert Helmert) Transform() Geographic {
    var x2, y2, z2 float64
    sourceDatum := helmert.Location.Datum()
    sourceECEF := helmert.Location.Convert()
    destinationDatum := helmert.DestinationDatum

    array := sourceDatum.Parameters(helmert.BaseTransformation.InstanceOf(destinationDatum, helmert.BaseTransformation.BaseDatum))

    x1 := sourceECEF.X().Meters()
    y1 := sourceECEF.Y().Meters()
    z1 := sourceECEF.Z().Meters()
    
    tx := array["tx"]
    ty := array["ty"]
    tz := array["tz"]
    s := array["s"]
    s1 := s/1e6 + 1
    rx := (unit.Angle(array["rx"]/3600) * unit.Degree).Radians()
    ry := (unit.Angle(array["ry"]/3600) * unit.Degree).Radians()
    rz := (unit.Angle(array["rz"]/3600) * unit.Degree).Radians()

    x2 = 0;
    y2 = 0;
    z2 = 0;

    if helmert.IsCFR {
        x2 += tx + x1*s1 + y1*rz - z1*ry
        y2 += ty - x1*rz + y1*s1 + z1*rx
        z2 += tz + x1*ry - y1*rx + z1*s1
    } else {
        x2 += tx + x1*s1 - y1*rz + z1*ry
        y2 += ty + x1*rz + y1*s1 - z1*rx
        z2 += tz - x1*ry + y1*rx + z1*s1
    }


    ECEF_new := Geocentric{}


    ECEF_new.SetDatum(destinationDatum)
    ECEF_new.SetX(unit.Length(x2) * unit.Meter)
    ECEF_new.SetY(unit.Length(y2) * unit.Meter)
    ECEF_new.SetZ(unit.Length(z2) * unit.Meter)

    return ECEF_new.Convert()
}