package transformation

import (
	"reflect"
    datum "bitbucket.org/jtejido/go-geodesy/Datum"
	"bitbucket.org/jtejido/go-geodesy/Unit"
)

type BaseTransformation struct {
	BaseUnit unit.Length
	BaseDegree unit.Angle
	BaseDatum datum.WGS84
}

func (basetransformation BaseTransformation) InstanceOf(objectPtr datum.DatumInterface, typePtr interface{}) bool {
  return reflect.TypeOf(objectPtr) == reflect.TypeOf(typePtr)
}
