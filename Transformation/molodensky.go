package transformation

import (
	"math"
	. "bitbucket.org/jtejido/go-geodesy/Location"
	datum "bitbucket.org/jtejido/go-geodesy/Datum"
    "bitbucket.org/jtejido/go-geodesy/Unit"
)

type Molodensky struct {
	Location Geographic
	DestinationDatum datum.DatumInterface
    BaseTransformation
}

func (molodensky Molodensky) Transform() Geographic {

    source := molodensky.Location

    sourceDatum := molodensky.Location.Datum()
    destinationDatum := molodensky.DestinationDatum

    // if converting to wgs84, use the inverse
    array := sourceDatum.Parameters(molodensky.BaseTransformation.InstanceOf(destinationDatum, molodensky.BaseTransformation.BaseDatum))

    fromF := sourceDatum.InverseFlattening()
    fromA := sourceDatum.SemiMajorAxis()
    fromEsq := sourceDatum.Model().FirstEccentricitySquared()
    dx := array["tx"]
    dy := array["ty"]
    dz := array["tz"]

    da := sourceDatum.SemiMajorAxis() - destinationDatum.SemiMajorAxis()
    df := sourceDatum.InverseFlattening() - destinationDatum.InverseFlattening()

	sinLat := math.Sin(source.Latitude().Radians())
    cosLat := math.Cos(source.Latitude().Radians())
    sinLong := math.Sin(source.Longitude().Radians())
    cosLong := math.Cos(source.Longitude().Radians())
    sinLatEsq := math.Pow(sinLat, 2)
    adb := 1.0 / (1.0 - fromF)
    h := source.Altitude().Meters()
    
    // radius of curvature in prime vertical
    rn := fromA / math.Sqrt(1.0 - fromEsq * sinLatEsq)

    // radius of curvature in prime meridian
    rm := fromA * (1 - fromEsq) / math.Pow((1.0 - fromEsq * sinLatEsq), 1.5)

    deltaLat := (((((-dx * sinLat * cosLong - dy * sinLat * sinLong) + dz * cosLat) + (da * ((rn * fromEsq * sinLat * cosLat) / fromA))) + (df * (rm * adb + rn / adb) * sinLat * cosLat))) / (rm + h)

    deltaLong := (-dx * sinLong + dy * cosLong) / ((rn + h) * cosLat)

    deltaHeight := (dx * cosLat * cosLong) + (dy * cosLat * sinLong) + (dz * sinLat) - (da * (fromA / rn)) + ((df * rn * sinLatEsq) / adb)

    latlong_new := Geographic{}

    latlong_new.SetDatum(destinationDatum)
    latlong_new.SetLatitude(unit.Angle(source.Latitude().Radians() + deltaLat) * unit.Degree)
    latlong_new.SetLongitude(unit.Angle(source.Longitude().Radians() + deltaLong) * unit.Degree)
    latlong_new.SetAltitude(unit.Length(source.Altitude().Meters() + deltaHeight) * unit.Meter)

    return latlong_new
}
