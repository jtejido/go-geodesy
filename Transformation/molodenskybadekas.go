package transformation

import (
	datum "bitbucket.org/jtejido/go-geodesy/Datum"
	. "bitbucket.org/jtejido/go-geodesy/Location"
	. "bitbucket.org/jtejido/go-geodesy/Unit/Angle"
)

type MolodenskyBadekas struct {
	Location         Geographic
	DestinationDatum datum.DatumInterface
	BaseTransformation
}

func (molodenskybadekas MolodenskyBadekas) Transform() Geographic {

	sourceDatum := molodenskybadekas.Location.Datum()
	sourceECEF := molodenskybadekas.Location.Convert()
	destinationDatum := molodenskybadekas.DestinationDatum

	// always inverse
	array := sourceDatum.Parameters(true)

	x1 := sourceECEF.X().Value()
	y1 := sourceECEF.Y().Value()
	z1 := sourceECEF.Z().Value()

	tx := array["tx"]
	ty := array["ty"]
	tz := array["tz"]
	x1_p := tx + x1
	y1_p := ty + y1
	z1_p := tz + z1

	s := array["s"]
	s1 := s/1e6 + 1
	rx := Degree{array["rx"] / 3600}.Radian()
	ry := Degree{array["ry"] / 3600}.Radian()
	rz := Degree{array["rz"] / 3600}.Radian()

	x2 := tx + ((x1 - x1_p) * s1) + ((y1 - y1_p) * rz) - ((z1 - z1_p) * ry) + x1_p
	y2 := ty - ((x1 - x1_p) * rz) + ((y1 - y1_p) * s1) + ((z1 - z1_p) * rx) + y1_p
	z2 := tz + ((x1 - x1_p) * ry) - ((y1 - y1_p) * rx) + ((z1 - z1_p) * s1) + z1_p

	ECEF_new := Geocentric{}
	XMetre := molodenskybadekas.BaseTransformation.BaseUnit
	XMetre.SetValue(x2)
	YMetre := molodenskybadekas.BaseTransformation.BaseUnit
	YMetre.SetValue(y2)
	ZMetre := molodenskybadekas.BaseTransformation.BaseUnit
	ZMetre.SetValue(z2)

	ECEF_new.SetDatum(destinationDatum)
	ECEF_new.SetX(XMetre)
	ECEF_new.SetY(YMetre)
	ECEF_new.SetZ(ZMetre)

	return ECEF_new.Convert()
}
