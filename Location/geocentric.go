package location

import (
	"math"
	datum "bitbucket.org/jtejido/go-geodesy/Datum"
	"bitbucket.org/jtejido/go-geodesy/Unit"
)


type Geocentric struct {
    GeocentricX, GeocentricY, GeocentricZ unit.Length
    GeocentricDatum datum.DatumInterface
}

func (geocentric *Geocentric) SetX(x unit.Length) {
	geocentric.GeocentricX = x
}

func (geocentric *Geocentric) SetY(y unit.Length) {
	geocentric.GeocentricY = y
}

func (geocentric *Geocentric) SetZ(z unit.Length) {
	geocentric.GeocentricZ = z
}

func (geocentric *Geocentric) SetDatum(datum datum.DatumInterface) {
	geocentric.GeocentricDatum = datum
}

func (geocentric Geocentric) X() unit.Length {
	return geocentric.GeocentricX
}

func (geocentric Geocentric) Y() unit.Length {
	return geocentric.GeocentricY
}

func (geocentric Geocentric) Z() unit.Length {
	return geocentric.GeocentricZ
}

func (geocentric Geocentric) Datum() datum.DatumInterface {
	return geocentric.GeocentricDatum
}

func (geocentric Geocentric) Convert() Geographic {

    x := geocentric.GeocentricX.Meters()
    y := geocentric.GeocentricY.Meters()
    z := geocentric.GeocentricZ.Meters()

    esq := geocentric.GeocentricDatum.Model().FirstEccentricitySquared()
    asq := math.Pow(geocentric.GeocentricDatum.SemiMajorAxis(), 2)
    b := math.Sqrt( asq * (1 - esq) )
    bsq := math.Pow(b, 2)
    ep := math.Sqrt( (asq - bsq) / bsq )
    p := math.Sqrt( math.Pow(x, 2) + math.Pow(y, 2) )
    th := math.Atan2(geocentric.GeocentricDatum.SemiMajorAxis() * z, b * p)

    long := math.Atan2(y,x) 
    lat := math.Atan2( (z + math.Pow(ep,2) * b * math.Pow(math.Sin(th), 3) ), (p - esq * (geocentric.GeocentricDatum.SemiMajorAxis() * math.Pow(math.Cos(th),3)) ))
    alt := p / math.Cos(lat) - geocentric.GeocentricDatum.SemiMajorAxis() / ( math.Sqrt(1 - esq * math.Pow(math.Sin(lat),2)) )


    geo := Geographic{
			GeographicDatum: geocentric.GeocentricDatum,
			GeographicLatitude: unit.Angle((unit.Angle(lat) * unit.Radian).Degrees()) * unit.Degree,
			GeographicLongitude: unit.Angle((unit.Angle(long) * unit.Radian).Degrees()) * unit.Degree,
			GeographicAltitude: unit.Length(alt) * unit.Meter }
	
	return geo
}