package location

import (
	"math"
	datum "bitbucket.org/jtejido/go-geodesy/Datum"
	"bitbucket.org/jtejido/go-geodesy/Unit"
)


type Geographic struct {
    GeographicLatitude, GeographicLongitude unit.Angle
    GeographicAltitude unit.Length
    GeographicDatum datum.DatumInterface
}

func (geographic *Geographic) SetLatitude(lat unit.Angle) {
	geographic.GeographicLatitude = lat
}

func (geographic *Geographic) SetLongitude(long unit.Angle) {
	geographic.GeographicLongitude = long
}

func (geographic *Geographic) SetAltitude(alt unit.Length) {
	geographic.GeographicAltitude = alt
}

func (geographic *Geographic) SetDatum(datum datum.DatumInterface) {
	geographic.GeographicDatum = datum
}

func (geographic Geographic) Latitude() unit.Angle {
	return geographic.GeographicLatitude
}

func (geographic Geographic) Longitude() unit.Angle {
	return geographic.GeographicLongitude
}

func (geographic Geographic) Altitude() unit.Length {
	return geographic.GeographicAltitude
}

func (geographic Geographic) Datum() datum.DatumInterface {
	return geographic.GeographicDatum
}

func (geographic Geographic) Convert() Geocentric {

    lat := geographic.GeographicLatitude.Radians()

    long := geographic.GeographicLongitude.Radians()

    alt := geographic.GeographicAltitude.Meters()

	esq := geographic.GeographicDatum.Model().FirstEccentricitySquared()

    n := geographic.GeographicDatum.Model().SemiMajorAxis() / math.Sqrt( 1 - (esq * math.Pow(math.Sin( lat ), 2)))
    
    geo := Geocentric{
    			GeocentricDatum: geographic.GeographicDatum,
    			GeocentricX: unit.Length((( n + alt ) * math.Cos( lat ) * math.Cos( long ))) * unit.Meter,
    			GeocentricY: unit.Length((( n + alt ) * math.Cos( lat ) * math.Sin( long ))) * unit.Meter,
    			GeocentricZ: unit.Length((( 1 - esq ) * n + alt ) * math.Sin( lat )) * unit.Meter}
	return geo
}

