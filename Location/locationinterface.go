package location


type LocationInterface interface {
	Convert() Geocentric
}
