package unitconversion

import (
	. "bitbucket.org/jtejido/go-geodesy/Unit/Length"
	"reflect"
)

type UnitConversion struct {
	FromValue, ToValue LengthInterface
}

func (unitconversion *UnitConversion) From(value LengthInterface) {
	unitconversion.FromValue = value
}

func (unitconversion *UnitConversion) To(value LengthInterface) {
	unitconversion.ToValue = value
}

func (unitconversion UnitConversion) Convert() float64 {
	a, b := unitconversion.FromValue, unitconversion.ToValue
	
	if instanceOf(a, Metre{}) {
		return b.Convert(a.Value())
	} else {
		v := a.ConvertToMetre()
		return b.Convert(v)
	}
}

func instanceOf(objectPtr LengthInterface, typePtr interface{}) bool {
  return reflect.TypeOf(objectPtr) == reflect.TypeOf(typePtr)
}