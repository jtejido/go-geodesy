package locationconversion

import (
	. "bitbucket.org/jtejido/go-geodesy/Location/location"
)

type LocationConversion struct {
	FromValue, ToValue LocationInterface
}

func (locationconversion *LocationConversion) From(value LocationInterface) {
	locationconversion.FromValue = value
}

func (locationconversion *LocationConversion) To(value LocationInterface) {
	locationconversion.ToValue = value
}

func (locationconversion LocationConversion) Convert() LocationInterface {
	return locationconversion.FromValue.Convert()
}
