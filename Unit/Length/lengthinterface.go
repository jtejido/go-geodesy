package length

type LengthInterface interface {
	Convert(value float64) float64
	ConvertToMetre() float64
	Value() float64
}
