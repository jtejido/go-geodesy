package length

type Metre struct {
	MetreValue float64
}

func (metre Metre) Convert(value float64) float64 {
	return value
}

func (metre Metre) ConvertToMetre() float64 {
	return metre.Convert(metre.MetreValue)
}

func (metre Metre) Value() float64 {
	return metre.MetreValue
}

func (metre *Metre) SetValue(value float64) {
	metre.MetreValue = value
}

func (metre *Metre) SetMetreValue(value float64) {
	metre.SetValue(value)
}