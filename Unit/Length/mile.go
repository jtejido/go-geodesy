package length

type Mile struct {
	MetreValue, MileValue float64
}

func (mile Mile) Convert(value float64) float64 {
	return value * 0.000621371
}

func (mile Mile) ConvertToMetre() float64 {
	return mile.MileValue / 0.000621371
}

func (mile Mile) Value() float64 {
	return mile.MileValue
}

func (mile *Mile) SetMetreValue(value float64) {
	mile.MetreValue = value
}

func (mile *Mile) SetValue(value float64) {
	mile.MileValue = value
}