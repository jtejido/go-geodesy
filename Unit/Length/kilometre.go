package length

type Kilometre struct {
	MetreValue, KilometreValue float64
}

func (kilometre Kilometre) Convert(value float64) float64 {
	return value * 0.001
}

func (kilometre Kilometre) ConvertToMetre() float64 {
	return kilometre.KilometreValue / 0.001
}

func (kilometre Kilometre) Value() float64 {
	return kilometre.KilometreValue
}

func (kilometre *Kilometre) SetMetreValue(value float64) {
	kilometre.MetreValue = value
}

func (kilometre *Kilometre) SetValue(value float64) {
	kilometre.KilometreValue = value
}