package length

type NauticalMile struct {
	MetreValue, NauticalMileValue float64
}

func (nauticalMilemile NauticalMile) Convert(value float64) float64{
	return value * 0.00053995663640604751
}

func (nauticalMilemile NauticalMile) ConvertToMetre() float64 {
	return nauticalMilemile.NauticalMileValue / 0.00053995663640604751
}

func (nauticalMilemile NauticalMile) Value() float64 {
	return nauticalMilemile.NauticalMileValue
}

func (nauticalMilemile *NauticalMile) SetMetreValue(value float64) {
	nauticalMilemile.MetreValue = value
}

func (nauticalMilemile *NauticalMile) SetValue(value float64) {
	nauticalMilemile.NauticalMileValue = value
}
