package length

type AstronomicalUnit struct {
	MetreValue, AstronomicalUnitValue float64
}

func (astronomicalunit AstronomicalUnit) Convert(value float64) float64 {
	return value * 6.6846e-12
}

func (astronomicalunit AstronomicalUnit) ConvertToMetre() float64 {
	return astronomicalunit.AstronomicalUnitValue / 6.6846e-12
}

func (astronomicalunit AstronomicalUnit) Value() float64 {
	return astronomicalunit.AstronomicalUnitValue
}

func (astronomicalunit *AstronomicalUnit) SetMetreValue(value float64) {
	astronomicalunit.MetreValue = value
}

func (astronomicalunit *AstronomicalUnit) SetValue(value float64) {
	astronomicalunit.AstronomicalUnitValue = value
}