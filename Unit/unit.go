package unit

import "math"

type Unit float64

type Length Unit

const (
	Meter            Length = 1e0
	Kilometer               = Meter * 1e3
	Mile    				= Meter * 1609.344
	NauticalMile 			= Meter * 1852
)


// Meters returns the length in m
func (l Length) Meters() float64 {
	return float64(l)
}

// Kilometers returns the length in km
func (l Length) Kilometers() float64 {
	return float64(l / Kilometer)
}

// Miles returns the length in mi
func (l Length) Miles() float64 {
	return float64(l / Mile)
}

// NauticalMiles returns the length in nm
func (l Length) NauticalMiles() float64 {
	return float64(l / NauticalMile)
}


type Angle Unit

const (
	Radian         Angle = 1e0
	Degree               = Radian * math.Pi / 180
)

// Radians returns the angle in ㎭
func (a Angle) Radians() float64 {
	return float64(a / Radian)
}

// Degrees returns the angle in °
func (a Angle) Degrees() float64 {
	return float64(a / Degree)
}

