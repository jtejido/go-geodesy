package angle

import (
	"math"
)

type Radian struct {
	RadianValue float64
}

func (radian *Radian) SetValue(value float64) {
	radian.RadianValue = value
}

func (radian Radian) Value() float64 {
	return radian.RadianValue
}

func (radian Radian) Degree() float64 {
	return rad2deg(radian.RadianValue)
}

func rad2deg(d float64) float64 {
	return d * 180.0 / math.Pi
}
