package angle

import (
	"math"
)

type Degree struct {
	DegreeValue float64
}

func (degree *Degree) SetValue(value float64) {
	degree.DegreeValue = value
}

func (degree Degree) Value() float64 {
	return degree.DegreeValue
}

func (degree Degree) Radian() float64 {
	return deg2rad(degree.DegreeValue)
}

func deg2rad(d float64) float64 {
	return d * math.Pi / 180.0
}
