package distance

import (
	"math"
    . "bitbucket.org/jtejido/go-geodesy/Unit/Length"
    . "bitbucket.org/jtejido/go-geodesy/Location"
)

type SphericalCosine struct {
    Source, Destination Geographic
    BaseDistance
}

func (sphericalcosine SphericalCosine) Distance() LengthInterface {

    lat1 := sphericalcosine.Source.Latitude().Radian()
    lat2 := sphericalcosine.Destination.Latitude().Radian()
    long1 := sphericalcosine.Source.Longitude().Radian()
    long2 := sphericalcosine.Destination.Longitude().Radian()
    a := sphericalcosine.BaseDistance.WGS84.SemiMajorAxis()
    b := sphericalcosine.BaseDistance.WGS84.SemiMinorAxis()

    R := ((2 * a) + b) / 3

    longDiff := long2 - long1
    d := math.Acos(math.Sin(lat1) * math.Sin(lat2) + math.Cos(lat1) * math.Cos(lat2) * math.Cos(longDiff)) * R
    
    Unit := sphericalcosine.BaseDistance.Unit
    Unit.SetValue(d)
    return Unit
}
