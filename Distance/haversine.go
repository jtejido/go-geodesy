package distance

import (
	"math"
    . "bitbucket.org/jtejido/go-geodesy/Unit/Length"
    . "bitbucket.org/jtejido/go-geodesy/Location"
)

type Haversine struct {
    Source, Destination Geographic
    BaseDistance
}

func (haversine Haversine) Distance() LengthInterface {

    lat1 := haversine.Source.Latitude().Radian()
    lat2 := haversine.Destination.Latitude().Radian()
    long1 := haversine.Source.Longitude().Radian()
    long2 := haversine.Destination.Longitude().Radian()
    a := haversine.BaseDistance.WGS84.SemiMajorAxis()
    b := haversine.BaseDistance.WGS84.SemiMinorAxis()

    r := ((2 * a) + b) / 3

    longDiff := long2 - long1
    latDiff := lat2 - lat1
    x := math.Pow(math.Sin(latDiff/2), 2) + math.Cos(lat1) * math.Cos(lat2) * math.Pow(math.Sin(longDiff/2), 2)
    y := 2 * math.Atan2(math.Sqrt(x), math.Sqrt(1 - x))
    d := y * r
    
    Unit := haversine.BaseDistance.Unit
    Unit.SetValue(d)
    return Unit
}
