package distance

import (
	"math"
    . "bitbucket.org/jtejido/go-geodesy/Unit/Length"
    . "bitbucket.org/jtejido/go-geodesy/Location"
)

type Hubeny struct {
    Source, Destination Geographic
    BaseDistance
}

func (hubeny Hubeny) Distance() LengthInterface {

	lat1 := hubeny.Source.Latitude().Radian()
    lat2 := hubeny.Destination.Latitude().Radian()
    long1 := hubeny.Source.Longitude().Radian()
    long2 := hubeny.Destination.Longitude().Radian()
    a := hubeny.BaseDistance.WGS84.SemiMajorAxis()
    b := hubeny.BaseDistance.WGS84.SemiMinorAxis()

    f2 := math.Pow(b, 2) / math.Pow(a, 2)
    e2 := 1 - f2

    latDiff := (lat1 - lat2)
    longDiff := (long1 - long2)
    latAverage := 0.5 * (lat1 + lat2)
    sinlatAverage := math.Sin(latAverage)
    coslatAverage := math.Cos(latAverage)
    w2 := 1 - math.Pow(sinlatAverage, 2) * e2
    w := math.Sqrt(w2)
    meridian := a * f2 / (w2 * w)
    n := a / w

    d := math.Sqrt(math.Pow(latDiff, 2) * math.Pow(meridian, 2) + math.Pow(longDiff, 2) * math.Pow(n, 2) * math.Pow(coslatAverage, 2))

    Unit := hubeny.BaseDistance.Unit
    Unit.SetValue(d)
    return Unit
}
