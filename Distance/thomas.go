package distance

import (
	"math"
    . "bitbucket.org/jtejido/go-geodesy/Unit/Length"
    . "bitbucket.org/jtejido/go-geodesy/Location"
)

type Thomas struct {
    Source, Destination Geographic
    BaseDistance
}

func (thomas Thomas) Distance() LengthInterface {

    lat1 := thomas.Source.Latitude().Radian()
    lat2 := thomas.Destination.Latitude().Radian()
    long1 := thomas.Source.Longitude().Radian()
    long2 := thomas.Destination.Longitude().Radian()
    A := thomas.BaseDistance.WGS84.SemiMajorAxis()
    f := thomas.BaseDistance.WGS84.InverseFlattening()

    ad := -long1
    ac := math.Atan((1.0-f) * math.Tan(lat1))

    af := -long2
    ae := math.Atan((1.0-f) * math.Tan(lat2))
    

    i := af - ad
    j := (ae + ac) / 2.0
    ak := (ae - ac) / 2.0
    h := math.Pow(math.Cos(ak), 2) - math.Pow(math.Sin(j), 2)

    l := math.Pow(math.Sin(ak), 2) + h * math.Pow(math.Sin(i/2.0), 2)
    d := 2.0 * math.Atan(math.Sqrt(l/(1.0-l)))
    
    u := 2.0 * math.Pow(math.Sin(j), 2) * math.Pow(math.Cos(ak), 2) / (1.0 - l)
    v := 2.0 * math.Pow(math.Sin(ak), 2) * math.Pow(math.Cos(j), 2) / l

    x := u + v
    y := u - v
    t := d / math.Sin(d)
    dd := 4.0 * t * t
    e := 2.0 * math.Cos(d)
    a := dd * e

    c := t - (a - e) / 2.0
    n1 := x * (a + c * x)
    b := 2.0 * dd
    n2 := y * (b + e * y)
    n3 := dd * x * y

    d1d := f * (t * x - y)/4.0
    d2d := f * f * (n1 - n2 + n3)/64.0

    D := A * (t - d1d + d2d) * math.Sin(d)

    Unit := thomas.BaseDistance.Unit
    Unit.SetValue(D)
    return Unit
}
