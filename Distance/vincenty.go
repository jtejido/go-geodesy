package distance

import (
	"fmt"
	"math"
    . "bitbucket.org/jtejido/go-geodesy/Unit/Length"
	. "bitbucket.org/jtejido/go-geodesy/Location"
)

type Vincenty struct {
    Source, Destination Geographic
    BaseDistance
}

func (vincenty Vincenty) Distance() LengthInterface {

    var cosSqAlpha, sinSigma, cos2SigmaM, cosSigma, sigma, sinLambda, lambda, lambdaPrime,
    cosLambda, sinSqSigma, sinAlpha, iterationCheck float64

    vincenty.Source = vincenty.BaseDistance.ToWGS(vincenty.Source)

    vincenty.Destination = vincenty.BaseDistance.ToWGS(vincenty.Destination)

	lat1 := vincenty.Source.Latitude().Radian()
    lat2 := vincenty.Destination.Latitude().Radian()
    long1 := vincenty.Source.Longitude().Radian()
    long2 := vincenty.Destination.Longitude().Radian()
    a := vincenty.BaseDistance.WGS84.SemiMajorAxis()
    b := vincenty.BaseDistance.WGS84.SemiMinorAxis()
    f := vincenty.BaseDistance.WGS84.InverseFlattening()
	L := long2 - long1

    tanU1 := (1-f) * math.Tan(lat1)

    tanU2 := (1-f) * math.Tan(lat2)

    cosU1 := 1 / math.Sqrt((1 + math.Pow(tanU1, 2) ))

    sinU1 := tanU1 * cosU1

    cosU2 := 1 / math.Sqrt((1 + math.Pow(tanU2, 2) ))

    sinU2 := tanU2 * cosU2

    lambda = L

    iterations := 100

    antimeridian := math.Abs(L) > math.Pi

    for math.Abs(lambda-lambdaPrime) > 1.0e-12 && iterations >= 0 {

    	iterations--

        sinLambda = math.Sin(lambda)

        cosLambda = math.Cos(lambda)

        sinSqSigma = math.Pow(cosU2*sinLambda, 2) + math.Pow(cosU1*sinU2-sinU1*cosU2*cosLambda, 2)

        if sinSqSigma == 0 {
        	fmt.Println("lambda is greater than pi")
        }

        sinSigma = math.Sqrt(sinSqSigma)

        cosSigma = sinU1*sinU2 + cosU1*cosU2*cosLambda

        sigma = math.Atan2(sinSigma, cosSigma)

        sinAlpha = cosU1 * cosU2 * sinLambda / sinSigma

        cosSqAlpha = 1 - math.Pow(sinAlpha, 2)

        cos2SigmaM = 0.0

        if cosSqAlpha != 0 {
        	cos2SigmaM = cosSigma - 2*sinU1*sinU2/cosSqAlpha
        }

        C := f/16*cosSqAlpha*(4+f*(4-3*cosSqAlpha))

        lambdaPrime = lambda

        lambda = L + (1-C) * f * sinAlpha * (sigma + C*sinSigma*(cos2SigmaM+C*cosSigma*(-1+2*cos2SigmaM*cos2SigmaM)))


        if antimeridian == true {
        	iterationCheck = math.Abs(lambda)-math.Pi
        } else {
        	iterationCheck = math.Abs(lambda)
        }

        if iterationCheck > math.Pi {
            fmt.Println("lambda is greater than pi")
        } 
    
    }

    if iterations == 0 {
        fmt.Println("Failed to converge")
    }

    uSq := cosSqAlpha * (math.Pow(a/b, 2) - 1)

    A := 1 + uSq/16384*(4096+uSq*(-768+uSq*(320-175*uSq)))
    B := uSq/1024 * (256+uSq*(-128+uSq*(74-47*uSq)))


    deltaSigma := B*sinSigma*(cos2SigmaM+B/4*(cosSigma*(-1+2*math.Pow(cos2SigmaM, 2))-
        B/6*cos2SigmaM*(-3+4*math.Pow(sinSigma, 2))*(-3+4* math.Pow(cos2SigmaM, 2))))

    d := b * A * (sigma-deltaSigma)

    Unit := vincenty.BaseDistance.Unit
    Unit.SetValue(d)
    return Unit

}
