package distance

import (
	"math"
    . "bitbucket.org/jtejido/go-geodesy/Unit/Length"
    . "bitbucket.org/jtejido/go-geodesy/Location"
)

type EllipticDistance struct {
    Source, Destination Geographic
    BaseDistance
}

func (ellipticdistance EllipticDistance) Distance() LengthInterface {

	lat1 := ellipticdistance.Source.Latitude().Radian()
    lat2 := ellipticdistance.Destination.Latitude().Radian()
    long1 := ellipticdistance.Source.Longitude().Radian()
    long2 := ellipticdistance.Destination.Longitude().Radian()
    a := ellipticdistance.BaseDistance.WGS84.SemiMajorAxis()
    f := ellipticdistance.BaseDistance.WGS84.InverseFlattening()
    F := (lat1 + lat2) / 2.0
    G := (lat1 - lat2) / 2.0
    L := (long1 - long2) / 2.0

    sing := math.Sin(G)
    cosl := math.Cos(L)
    cosf := math.Cos(F)
    sinl := math.Sin(L)
    sinf := math.Sin(F)
    cosg := math.Cos(G)

    S := sing*sing*cosl*cosl + cosf*cosf*sinl*sinl
    C := cosg*cosg*cosl*cosl + sinf*sinf*sinl*sinl
    W := math.Atan2(math.Sqrt(S),math.Sqrt(C))
    R := math.Sqrt((S*C))/W
    H1 := (3 * R - 1.0) / (2.0 * C)
    H2 := (3 * R + 1.0) / (2.0 * S)
    D := 2 * W * a
    d := D * (1 + f * H1 * sinf*sinf*cosg*cosg - f*H2*cosf*cosf*sing*sing)
    Unit := ellipticdistance.BaseDistance.Unit
    Unit.SetValue(d)
    return Unit
}
