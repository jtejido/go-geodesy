package distance

import (
	"math"
    . "bitbucket.org/jtejido/go-geodesy/Unit/Length"
    . "bitbucket.org/jtejido/go-geodesy/Location"
)

type ForsytheCorrection struct {
    Source, Destination Geographic
    BaseDistance
}

func (forsythecorrection ForsytheCorrection) Distance() LengthInterface {

    lat1 := forsythecorrection.Source.Latitude().Radian()
    lat2 := forsythecorrection.Destination.Latitude().Radian()
    long1 := forsythecorrection.Source.Longitude().Radian()
    long2 := forsythecorrection.Destination.Longitude().Radian()
    a := forsythecorrection.BaseDistance.WGS84.SemiMajorAxis()
    f := forsythecorrection.BaseDistance.WGS84.InverseFlattening()

    longDiff := long2 - long1
    sphericalDistance := math.Acos(math.Sin(lat1) * math.Sin(lat2) + math.Cos(lat1) * math.Cos(lat2) * math.Cos(longDiff))

    // this is the Forsythe Correction for Spherical Cosine Law
    
    P := math.Pow(math.Sin(lat1) + math.Sin(lat2), 2) / (1 + math.Cos(sphericalDistance))
    Q := math.Pow(math.Sin(lat1) - math.Sin(lat2), 2) / (1 - math.Cos(sphericalDistance))
    X := P + Q
    Y := P - Q
    deltaD1 := -( (X * sphericalDistance) - (3 * Y * math.Sin(sphericalDistance))) / 4

    A := (64*sphericalDistance) + ((16 * math.Pow(sphericalDistance, 2)) / math.Tan(sphericalDistance))
    D := (48*math.Sin(sphericalDistance)) + ((8 * (math.Pow(sphericalDistance, 2))) / math.Sin(sphericalDistance))
    B := -2*D
    E := 30 * math.Sin(2 * sphericalDistance)
    C := -((30 * sphericalDistance) + (8 * math.Pow(sphericalDistance, 2) / math.Tan(sphericalDistance)) + (E/2))
    deltaD2 := f * (A * X + B * Y + C * math.Pow(X, 2) + D * X * Y + E * math.Pow(Y, 2)) / 128

    delta := deltaD1 + deltaD2

    d := a * (sphericalDistance + (f * delta))

    Unit := forsythecorrection.BaseDistance.Unit
    Unit.SetValue(d)
    return Unit
}
