package distance

import (
	"math"
    . "bitbucket.org/jtejido/go-geodesy/Unit/Length"
    . "bitbucket.org/jtejido/go-geodesy/Location"
)

type AndoyerLambert struct {
    Source, Destination Geographic
    BaseDistance
}

func (andoyerlambert AndoyerLambert) Distance() LengthInterface {

	lat1 := andoyerlambert.Source.Latitude().Radian()
    lat2 := andoyerlambert.Destination.Latitude().Radian()
    long1 := andoyerlambert.Source.Longitude().Radian()
    long2 := andoyerlambert.Destination.Longitude().Radian()
    a := andoyerlambert.BaseDistance.WGS84.SemiMajorAxis()
    b := andoyerlambert.BaseDistance.WGS84.SemiMinorAxis()
    f := andoyerlambert.BaseDistance.WGS84.InverseFlattening()

    longDiff := long2 - long1
    rLat1 := math.Atan(b*math.Tan(lat1)/a)
    rLat2 := math.Atan(b*math.Tan(lat2)/a)
    sphericalDistance := math.Acos(math.Sin(rLat1) * math.Sin(rLat2) + math.Cos(rLat1) * math.Cos(rLat2) * math.Cos(longDiff))

    // this is the Andoyer-Lambert Correction for Spherical Cosine Law
    
    cosineSd := math.Cos(sphericalDistance/2.0)
    sinSd := math.Sin(sphericalDistance/2.0)
    c := (math.Sin(sphericalDistance) - sphericalDistance) * (math.Sin(rLat1)+math.Sin(rLat2))*(math.Sin(rLat1)+math.Sin(rLat2)) / math.Pow(cosineSd, 2)
    s := (math.Sin(sphericalDistance) + sphericalDistance) * (math.Sin(rLat1)-math.Sin(rLat2))*(math.Sin(rLat1)-math.Sin(rLat2)) / math.Pow(sinSd,2)
    delta := f / 8.0 * (c-s)

    d := a * (sphericalDistance+delta)
    Unit := andoyerlambert.BaseDistance.Unit
    Unit.SetValue(d)
    return Unit
}
