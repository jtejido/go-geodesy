package distance

import (
	"reflect"
	. "bitbucket.org/jtejido/go-geodesy/Unit/Length"
    datum "bitbucket.org/jtejido/go-geodesy/Datum"
    . "bitbucket.org/jtejido/go-geodesy/Transformation"
    . "bitbucket.org/jtejido/go-geodesy/Location"
)

type BaseDistance struct {
    Unit Metre
    WGS84 datum.WGS84
}

func (basedistance BaseDistance) ToWGS(position Geographic) Geographic {
  	isDatumWGS := basedistance.instanceOf(position.Datum(), basedistance.WGS84)
    if !isDatumWGS {
        return MolodenskyBadekas{Location: position, DestinationDatum: basedistance.WGS84}.Transform()
    } else {
    	return position
    }
}

func (basedistance BaseDistance) instanceOf(objectPtr datum.DatumInterface, typePtr interface{}) bool {
  return reflect.TypeOf(objectPtr) == reflect.TypeOf(typePtr)
}
